import React from 'react';
import { connect } from 'react-redux';
import { addTodo ,deleteTodo,toggleTodo } from '../actions';
import { cloneDeep, concat, filter, forEach, isEmpty, map, parseInt } from 'lodash';
//import uuid from 'node-uuid';

class Home extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			todoText:[ ],
			//nextTodoId:-1,
		};
		const uuidv1 = require('uuid/v1');
		this.addTodo=this.addTodo.bind(this);
	}


	addTodo(){
		let name = this.refs.txtTodoName.value;
		const uuidv1 = require('uuid/v1');
		console.log(this.props);
					this.props.addTodo({
					todoElem:name,
					todoId:uuidv1(),
					toggled:false

				});

					this.refs.txtTodoName.value="";
	}

	deleteTodo(todoId){
		/*
		let newTodos = this.props.todos[todoId];
		let nextTodoId=nextTodoId=this.props.todos[todoId].nextTodoId;
		this.props.deleteTodo(newTodos,nextTodoId);
		*/

		let todoId=uuidv1();
		let newTodos = this.props.todos[todoId];

		this.props.deleteTodo(newTodos, todoId);

	}

	toggleTodo(id){
		/*
			let newTodos = this.props.todos[id];
			let nextTodoId=this.props.todos[id].nextTodoId;
			this.props.toggleTodo(newTodos,nextTodoId);
			*/
			this.props.toggleTodo({
				todoElem:id,
				todoId: uuidv1(),
				toggled:!todo.toggled
			});
	}

	showTodo(){
		const todos=this.props.todos;
		return(
			<ul>
			{
				todos.map((todo,index) => {
					return (
						<li key={index}>
							<p  style={{textDecoration: todos[index].toggled ? 'line-through':'none' }}>{todo.todoElem}</p>
							<button onClick={ ()=>this.toggleTodo(todo.todoId)}> toggle </button>
							<button onClick={ ()=> this.deleteTodo(index)}>remove</button>
						</li>
					)
				})
			}
			</ul>
		)
	}

	render() {
		return (
			<div className="App">
				<input placeholder="I have to..."  ref="txtTodoName"    />
				<button type="button" onClick={ this.addTodo }>Add Todo</button>
				<div>
				{ this.showTodo() }
				 </div>

			</div>
		);
    }
}

function mapStateToProps(state){
	return {
		todos:state
	}
}

export default  connect(mapStateToProps,{ addTodo ,deleteTodo,toggleTodo})(Home);
